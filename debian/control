Source: openssh-ssh1
Section: net
Priority: optional
Maintainer: Debian OpenSSH Maintainers <debian-ssh@lists.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 libpam0g-dev | libpam-dev,
 libselinux1-dev [linux-any],
 libssl-dev,
 zlib1g-dev,
Standards-Version: 3.9.8
Uploaders:
 Colin Watson <cjwatson@debian.org>,
 Matthew Vernon <matthew@debian.org>,
Homepage: https://www.openssh.com/
Vcs-Git: https://salsa.debian.org/ssh-team/openssh-ssh1.git
Vcs-Browser: https://salsa.debian.org/ssh-team/openssh-ssh1
Rules-Requires-Root: no
X-Style: black

Package: openssh-client-ssh1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: foreign
Description: secure shell (SSH) client for legacy SSH1 protocol
 This is the portable version of OpenSSH, a free implementation of
 the Secure Shell protocol as specified by the IETF secsh working
 group.
 .
 Ssh (Secure Shell) is a program for logging into a remote machine
 and for executing commands on a remote machine.
 It provides secure encrypted communications between two untrusted
 hosts over an insecure network. X11 connections and arbitrary TCP/IP
 ports can also be forwarded over the secure channel.
 It can be used to provide applications with a secure communication
 channel.
 .
 This package provides the ssh1 and scp1 clients and the ssh-keygen1
 utility, all built with support for the legacy SSH1 protocol. This
 protocol is obsolete and should not normally be used, but in some cases
 there may be no alternative way to connect to outdated servers.
 .
 In some countries it may be illegal to use any encryption at all
 without a special permit.
 .
 ssh replaces the insecure rsh, rcp and rlogin programs, which are
 obsolete for most purposes.
